﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace XamSample.Mvvm
{

    public class LoginViewController
    {
        private LoginView _loginView;

        public LoginViewController(LoginView view)
        {
            _loginView = view;
            _loginView.Login.Clicked += Login_Clicked;
            _loginView.SignUp.Clicked += SignUp_Clicked;
            _loginView.UserName.TextChanged += UserName_TextChanged;
        }

        void Login_Clicked(object sender, EventArgs e)
        {
            // TODO: Login
            _loginView.Result.Text = "Successfully Logged In!";
        }

        void SignUp_Clicked(object sender, EventArgs e)
        {
            // TODO: Navigate to SignUp
        }

        void UserName_TextChanged(object sender, TextChangedEventArgs e)
        {
            // TODO: Validate
        }
    }
}
