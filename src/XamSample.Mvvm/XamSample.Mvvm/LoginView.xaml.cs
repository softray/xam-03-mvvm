﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace XamSample.Mvvm
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class LoginView : ContentPage
    {   
        private LoginViewController _controller;
        
        public LoginView()
        {
            InitializeComponent();
            _controller = new LoginViewController(this);
        }
        
        internal Entry UserName { get { return this.usernameEntry; } }
        
        internal Entry Password { get { return this.passwordEntry; } }
        
        internal Label Result { get { return this.messageLabel; } }
        
        internal Button Login { get { return this.loginButton; } }
        
        internal ToolbarItem SignUp { get { return this.signUpButton; } }
    }
}
