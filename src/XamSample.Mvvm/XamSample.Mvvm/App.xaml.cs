﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamSample.Mvvm.Services;
using XamSample.Mvvm.Services.Abstractions;

namespace XamSample.Mvvm
{
    public partial class App : Application
    {
        private readonly IErrorSinkService _errorSink;

        public App()
        {
            InitializeComponent();

            DependencyService.Register<IAuthenticationService, MockAuthenticateService>();

            _errorSink = new MockErrorSinkService();

            MainPage = new NavigationPage(new AuthenticateView());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
