﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using XamSample.Mvvm.Services.Abstractions;

namespace XamSample.Mvvm
{
    public class AuthenticateViewModel : INotifyPropertyChanged

    {
        private string _userName;

        private string _password;

        private string _result;

        private Command _loginCommand;

        private readonly IAuthenticationService _authenticationService;

        public AuthenticateViewModel()
        {
            _loginCommand = new Command(Login, Validate);

            _authenticationService = DependencyService.Resolve<IAuthenticationService>();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public string UserName
        {
            get { return _userName; }
            set
            {
                if (_userName != value)
                {
                    _userName = value;
                    _loginCommand.ChangeCanExecute();
                }
            }
        }

        public string Password
        {
            get { return _password; }
            set
            {
                if (_password != value)
                {
                    _password = value;
                    _loginCommand.ChangeCanExecute();
                }
            }
        }

        public string Result
        {
            get { return _result; }
            set
            {
                if (_result != value)
                {
                    _result = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Result)));
                }
            }
        }

        public ICommand LoginCommand
        {
            get { return _loginCommand; }
        }

        //public void LoginSimple()
        //{
        //    var result = _authenticationService.Authenticate(UserName, Password);

        //    if (result)
        //    {
        //        Result = "Successfully Logged In";
        //    }
        //    else
        //    {
        //        Result = "Login Failed";
        //    }
        //}

        public void Login()
        {
            var result = false;

            try
            {
                result = _authenticationService.Authenticate(UserName, Password);


            }
            catch (Exception ex)
            {
                MessagingCenter.Send(this, "ServiceError", ex.Message);
            }

            if (result)
            {
                Result = "Successfully Logged In";
            }
            else
            {
                Result = "Login Failed";
            }
        }


        public bool Validate()
        {
            return !string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(Password);
        }
    }
}
