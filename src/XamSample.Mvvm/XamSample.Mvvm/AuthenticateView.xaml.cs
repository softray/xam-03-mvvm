﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamSample.Mvvm
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AuthenticateView : ContentPage
    {
        public AuthenticateView()
        {
            InitializeComponent();

            BindingContext = new AuthenticateViewModel();
        }
    }
}