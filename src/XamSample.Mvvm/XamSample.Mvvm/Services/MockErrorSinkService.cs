﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using XamSample.Mvvm.Services.Abstractions;

namespace XamSample.Mvvm.Services
{
    public class MockErrorSinkService : IErrorSinkService
    {
        public MockErrorSinkService()
        {
            MessagingCenter.Subscribe<AuthenticateViewModel, string>(this, "ServiceError", (sender, arg) =>
            {
                System.Diagnostics.Debug.WriteLine(arg);
            });
        }
    }
}
