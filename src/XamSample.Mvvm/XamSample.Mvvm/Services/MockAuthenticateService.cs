﻿using System;
using System.Collections.Generic;
using System.Text;
using XamSample.Mvvm.Services.Abstractions;

namespace XamSample.Mvvm.Services
{
    public class MockAuthenticateService : IAuthenticationService
    {
        public bool Authenticate(string username, string password)
        {
            if (username == "test")
            {
                throw new ArgumentException();
            }

            if (username != password)
            {
                return true;
            }

            return false;
        }
    }
}
